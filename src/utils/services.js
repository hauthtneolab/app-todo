export const getTaskList = () => {
  return JSON.parse(localStorage.getItem('taskList')) || [];
}

export const setTaskList = (taskList = []) => {
  localStorage.setItem('taskList', JSON.stringify(taskList));
}

export const getGroupList = () => {
  return JSON.parse(localStorage.getItem('groupList')) || [];
}

export const setGroupList = (groupList = []) => {
  localStorage.setItem('groupList', JSON.stringify(groupList));
}